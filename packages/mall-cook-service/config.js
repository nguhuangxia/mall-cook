/*
 * @Description: 配置信息
 * @Autor: WangYuan
 * @Date: 2022-02-10 19:20:33
 * @LastEditors: WangYuan
 * @LastEditTime: 2022-11-02 10:35:50
 */
config = {
  appid: 'wxe62ed124a952a697', // 小程序appId
  secret: 'de78b70b9a3228c0878614e05734df4f', // 小程序secret
  serviceApi: 'http://127.0.0.1:3000', // 服务器地址
  //mongodbUrl: 'mongodb://localhost:27017/mall-cook', // mongodb数据库地址 格式：mongodb://username:password@host:port/name
  mongodbUrl: 'mongodb://127.0.0.1:27017/mall-cook',
  jwtSecret: 'secret'
}

module.exports = config

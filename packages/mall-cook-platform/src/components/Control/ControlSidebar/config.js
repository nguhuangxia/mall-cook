export function customLayerItem(list=[]) {
	return list.map(v => {
		return {
			...v,
			label: v.component === 'McTitle' ? v.value.title : v.name,
			value: v.id
		}
	})
}